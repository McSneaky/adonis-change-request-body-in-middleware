'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class ChangeField {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request }, next) {
    request.body = {
      ...request.body,
      test: 'This will be modified'
    }
    request.stuff = 'Get this with request.stuff not request.all()'
    await next()
  }
}

module.exports = ChangeField
